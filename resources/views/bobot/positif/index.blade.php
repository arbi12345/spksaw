<?php 
use App\Models\Bobot;
 ?>
@extends('index')
@section('title', 'Memiliki pikiran positif')
@section('content')
@if(Session('add'))
<script type="text/javascript">
    $(window).on('load',function(){
        Swal.fire({
        icon: 'success',
        title: 'Ok...',
        text: 'Data Berhasil Ditambahkan!',
        footer: '<a href>Tutup</a>'
        });
    });
</script>
@endif
@if(Session('edit'))
  <script type="text/javascript">
      // alert("Password Salah!");
      $(window).on('load',function(){
          Swal.fire({
          icon: 'success',
          title: 'Ok...',
          text: 'Data Berhasil Dirubah!',
          footer: '<a href>Tutup</a>'
          });
      });
  </script>
@endif

<!-- <button onclick="return getStudents(1);">Test</button> -->
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Memiliki pikiran positif</h4>
          <button type="button" class="btn btn-info btn-rounded btn-fw" data-toggle="modal" data-target="#add" style="float: right;margin-bottom: 15px;">Tambah</button>
          <table class="table table-striped" id="data_admin">
              <thead>
                  <tr>
                      <th> Nilai </th>
                      <th> Nama </th>
                      <th> Aksi </th>
                  </tr>
              </thead>
          </table>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Bobot</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{url('/bobot/positif/add')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="form-group">
            <label>Nilai</label>
            <select class="select2 form-control" name="bobot_id">
              @foreach(Bobot::all() as $bobot)
                <option value="{{$bobot->id}}">{{$bobot->nama}} ( {{$bobot->nilai}} )</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Tambah">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="add()" class="btn btn-success">Tambah</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="form_category" action="{{url('/bobot/positif/edit')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="id" id="id_edit">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="name" class="form-control" id="name_edit">
          </div>
          <div class="form-group">
            <label>Nilai</label>
            <select class="select2 form-control" name="bobot_id" id="bobot_id_edit">
              @foreach(Bobot::all() as $bobot)
                <option value="{{$bobot->id}}">{{$bobot->nama}} ( {{$bobot->nilai}} )</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Edit">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="edit()" class="btn btn-success">Edit</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
      $('#data_admin').DataTable({
          processing: true,
          serverSide: true,
          stateSave: true,
          ajax: "{{url('/bobot/positif/get')}}",
          oLanguage: {sProcessing: "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif' /><P style='margin-top:5px;color:blue;size:18px;'>Loading......</p>"},
          columns: [
              {
                  data: 'nilai',
                  name: 'nilai',
                  searchable: false
              },
              {
                  data: 'name',
                  name: 'name',
                  searchable: true
              },
              {
                  data: 'action',
                  name: 'action',
                  searchable: false
              }
          ]
      });
  });
</script>
<script>
    function edit(id, name, bobot_id) {
        $('#id_edit').val(id);
        $('#name_edit').val(name);
        $('#bobot_id_edit').val(bobot_id);
        $('#edit').modal('show');
    }
</script>
<script>
    function hapus(id) {
    
        Swal.fire({
            title: "Apakah Anda Yakin?",
            text: "Data tidak akan bisa dikembalikan lagi!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya yakin!'
            }).then((result) => {
            if (result.value) {
                var url = "{{url('/bobot/positif/delete')}}";
                $.post( url, { _token: "{{ csrf_token() }}", id: id })
                .done(function( data ) {
                    if (data.error) {
                        Swal.fire("Gagal", "Penghapusan gagal, Kesalahan DB!", "error");
                    } else {
                        Swal.fire("Berhasil", "Penghapusan berhasil!", "success");
                        location.reload();
                    }
                });
            } else {
                Swal.fire("Batalkan", "Penghapusan Dibatalkan!", "error");
            }
        });        
    }
</script>
@endsection