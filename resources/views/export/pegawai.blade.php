<!DOCTYPE html>
<html>
<head>
	<title>Laporan Pegawai</title>
</head>
<style type="text/css">
	table, tr, th, td {
		border: 1px solid black;
	}
</style>
<body style="width: 100%;">
	<table width="100%">
		<tr>
			<td>No.</td>
			<td>Nama</td>
			<td>Username</td>
			<td>Email</td>
		</tr>
		<?php $no=1; ?>
		@foreach($users as $user)
		<tr>
			<td>{{$no++}}</td>
			<td>{{$user->nama}}</td>
			<td>{{$user->username}}</td>
			<td>{{$user->email}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>