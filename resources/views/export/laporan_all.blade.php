<?php 
use App\Models\User;
use App\Models\Penilaian;
use App\Models\Periode;
use App\Models\Bobot;
 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Tunggal</title>
</head>
<body>

<table class="table table-dashed table-responsive" style="width: 100%;">
  <tr>
    <td rowspan="2">NO</td>
    <td rowspan="2">Nama</td>
    <td rowspan="2">Periode</td>
    <td colspan="4">Kesesuaian dengan bidang ilmu</td>
    <td colspan="5">Hasil ujian tertulis psikotest</td>
    <td colspan="5">Hasil wawancara</td>
    <td colspan="4">Penampilan</td>
    <td rowspan="2">Total</td>
  </tr>
  <tr>
    <td>Pendidikan</td>
    <td>IPK</td>
    <td>Kesesuaian</td>
    <td>Pengalaman</td>
    <td>IQ</td>
    <td>Kepribadian</td>
    <td>Gambar</td>
    <td>Kraeplein</td>
    <td>Wartegg</td>
    <td>Mental</td>
    <td>Positif</td>
    <td>Visi</td>
    <td>Inovasi</td>
    <td>Pantang Menyerah</td>
    <td>Tinggi Badan</td>
    <td>Usia</td>
    <td>Penampilan</td>
    <td>Tanggungan</td>
  </tr>
  <?php 
  $no = 1;
  if ($periode == '0') {
    $penilaian = Penilaian::orderBy('periode_id')->orderBy('user_id')->get();
  } else {
    $penilaian = Penilaian::where('periode_id', $periode)->orderBy('periode_id')->orderBy('user_id')->get();
  }
   ?>
   @foreach($penilaian as $penilaian)
    <tr>
      <td>{{$no++}}</td>
      <td>{{User::find($penilaian->user_id)->nama}}</td>
      <td>{{Periode::find($penilaian->periode_id)->tahun}}</td>
      <td>{{$pendidikan = Bobot::find($penilaian->pendidikan)->nilai}}</td>
      <td>{{$ipk = Bobot::find($penilaian->ipk)->nilai}}</td>
      <td>{{$kesesuaian = Bobot::find($penilaian->kesesuaian)->nilai}}</td>
      <td>{{$pengalaman = Bobot::find($penilaian->pengalaman)->nilai}}</td>
      <td>{{$iq = Bobot::find($penilaian->iq)->nilai}}</td>
      <td>{{$kepribadian = Bobot::find($penilaian->kepribadian)->nilai}}</td>
      <td>{{$gambar = Bobot::find($penilaian->gambar)->nilai}}</td>
      <td>{{$kraep = Bobot::find($penilaian->kraep)->nilai}}</td>
      <td>{{$wartegg = Bobot::find($penilaian->wartegg)->nilai}}</td>
      <td>{{$mental = Bobot::find($penilaian->mental)->nilai}}</td>
      <td>{{$positif = Bobot::find($penilaian->positif)->nilai}}</td>
      <td>{{$visi = Bobot::find($penilaian->visi)->nilai}}</td>
      <td>{{$inovasi = Bobot::find($penilaian->inovasi)->nilai}}</td>
      <td>{{$pantang = Bobot::find($penilaian->pantang)->nilai}}</td>
      <td>{{$tinggi = Bobot::find($penilaian->tinggi)->nilai}}</td>
      <td>{{$usia = Bobot::find($penilaian->usia)->nilai}}</td>
      <td>{{$penampilan = Bobot::find($penilaian->penampilan)->nilai}}</td>
      <td>{{$tanggungan = Bobot::find($penilaian->tanggungan)->nilai}}</td>
      <?php 
      $pendidikan = ($pendidikan * 20) / 100;
      $ipk = ($ipk * 20) / 100;
      $kesesuaian = ($kesesuaian * 30) / 100;
      $pengalaman = ($pengalaman * 30) / 100;

      $item1 = $pendidikan + $ipk + $kesesuaian + $pengalaman;
      $item1 = ($item1 * 30) / 100;

      $iq = ($iq * 20) / 100;
      $kepribadian = ($kepribadian * 25) / 100;
      $gambar = ($gambar * 25) / 100;
      $kraep = ($kraep * 20) / 100;
      $wartegg = ($wartegg * 10) / 100;

      $item2 = $iq + $kepribadian + $gambar + $kraep + $wartegg;
      $item2 = ($item2 * 20) / 100;

      $mental = ($mental * 30) / 100;
      $positif = ($positif * 20) / 100;
      $visi = ($visi * 10) / 100;
      $inovasi = ($inovasi * 20) / 100;
      $pantang = ($pantang * 20) / 100;

      $item3 = $mental + $positif + $visi + $inovasi + $pantang;
      $item3 = ($item2 * 40) / 100;

      $mental = ($mental * 30) / 100;
      $positif = ($positif * 20) / 100;
      $visi = ($visi * 10) / 100;
      $inovasi = ($inovasi * 20) / 100;
      $pantang = ($pantang * 20) / 100;

      $item3 = $mental + $positif + $visi + $inovasi + $pantang;
      $item3 = ($item3 * 20) / 100;

      $usia = ($usia * 50) / 100;
      $tinggi = ($tinggi * 10) / 100;
      $penampilan = ($penampilan * 20) / 100;
      $tanggungan = ($tanggungan * 20) / 100;

      $item4 = $usia + $tinggi + $penampilan + $tanggungan;
      $item4 = ($item4 * 10) / 100;

      $items = $item1 + $item2 + $item3 + $item4;
      $persentase = $items * 100;
      ?>
      <td>{{$items}} ({{$persentase}}%)</td>
    </tr>
   @endforeach
</table>
</body>
</html>