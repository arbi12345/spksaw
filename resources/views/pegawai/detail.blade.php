@extends('index')
@section('title', 'Detail Pegawai')
@section('content')
<div class="row">
  <div class="col-lg-12">
   <form method="post" action="{{url('/pegawai/edit')}}" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="id" value="{{$user->id}}">
      <div class="form-group">
        <label>Nama</label>
        <input type="text" name="name" class="form-control" value="{{$user->nama}}">
      </div>
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" value="{{$user->username}}">
      </div>
      <div class="form-group">
        <label>Alamat Email</label>
        <input type="text" name="email" class="form-control" value="{{$user->email}}">
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="text" name="password" class="form-control" value="" placeholder="Kosongkan Jika Tetap!">
      </div>
      <div class="form-group">
        <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Edit">
      </div>
   </form>
  </div>
</div>
@endsection
@section('js')
@if(session('error'))
    @if(session('error') == 'password')
        <script type="text/javascript">
            $(window).on('load',function(){
                Swal.fire({
                  type: 'success',
                  title: 'OK...',
                  text: 'Data Diperbaharui!'
                });
            });
        </script>
    @endif
@endif
@endsection