<?php 
use App\Models\Periode;
?>
@extends('index')
@section('title', 'Daftar Pegawai')
@section('content')
@if(Session('add'))
<script type="text/javascript">
    $(window).on('load',function(){
        Swal.fire({
        icon: 'success',
        title: 'Ok...',
        text: 'Data Berhasil Ditambahkan!',
        footer: '<a href>Tutup</a>'
        });
    });
</script>
@endif
@if(Session('edit'))
  <script type="text/javascript">
      // alert("Password Salah!");
      $(window).on('load',function(){
          Swal.fire({
          icon: 'success',
          title: 'Ok...',
          text: 'Data Berhasil Dirubah!',
          footer: '<a href>Tutup</a>'
          });
      });
  </script>
@endif

<!-- <button onclick="return getStudents(1);">Test</button> -->
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pegawai</h4>
          <button type="button" class="btn btn-info btn-rounded btn-fw" data-toggle="modal" data-target="#download" style="float: right;margin-right: 15px;margin-bottom: 15px;">Download</button>
          <button type="button" class="btn btn-info btn-rounded btn-fw" data-toggle="modal" data-target="#upload" style="float: right;margin-right: 15px;margin-bottom: 15px;">Upload</button>
          <button type="button" class="btn btn-info btn-rounded btn-fw" data-toggle="modal" data-target="#add" style="float: right;margin-right: 15px;margin-bottom: 15px;">Tambah</button>
          <table class="table table-striped" id="data_pegawai">
              <thead>
                  <tr>
                      <th> Nama </th>
                      <th> Aksi </th>
                  </tr>
              </thead>
          </table>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{url('/pegawai/add')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control">
          </div>
          <div class="form-group">
            <label>Alamat Email</label>
            <input type="text" name="email" class="form-control">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control">
          </div>
          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Tambah">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="add()" class="btn btn-success">Tambah</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="form_category" action="{{url('/pegawai/edit')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="id" id="id_edit">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="name" class="form-control" id="name_edit">
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" id="username_edit">
          </div>
          <div class="form-group">
            <label>Alamat Email</label>
            <input type="text" name="email" class="form-control" id="email_edit">
          </div>
          <div class="form-group">
            <label>Password</label><sup style="color: red;">*</sup>
            <input type="password" name="password" class="form-control" id="password_edit">
          </div>
          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Edit">
          </div>
        </form>
        <p style="color:red;">*) Kosongkan jika tidak diubah!</p>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="edit()" class="btn btn-success">Edit</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="download">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Download</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="form_category" action="{{url('/pegawai/download')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group">
            <label>Format</label>
            <select class="form-control select2" name="format">
              <option value="pdf">PDF</option>
              <option value="excel">EXCEL</option>
            </select>
          </div>
          
          <!-- <div class="form-group">
            <label>Periode</label>
            <select class="form-control select2" name="periode">
              <option value="0">Semua</option>
               @foreach(Periode::all() as $periode)
                <option value="{{$periode->id}}">{{$periode->nama}}</option>
               @endforeach
            </select>
          </div> -->

          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Download">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="edit()" class="btn btn-success">Edit</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>


<!-- The Modal -->
<div class="modal" id="upload">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Upload Excel</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="form_category" action="{{url('/pegawai/upload')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group">
            <label>File</label>
            <input type="file" name="import">
          </div>
          
          <!-- <div class="form-group">
            <label>Periode</label>
            <select class="form-control select2" name="periode">
              <option value="0">Semua</option>
               @foreach(Periode::all() as $periode)
                <option value="{{$periode->id}}">{{$periode->nama}}</option>
               @endforeach
            </select>
          </div> -->

          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Upload">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="edit()" class="btn btn-success">Edit</button> -->
        <a type="button" class="btn btn-danger" href="{{url('/pegawai/export')}}">Download Format Excel</a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
      $('#data_pegawai').DataTable({
          processing: true,
          serverSide: true,
          stateSave: true,
          ajax: "{{url('/pegawai/get')}}",
          oLanguage: {sProcessing: "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif' /><P style='margin-top:5px;color:blue;size:18px;'>Loading......</p>"},
          columns: [
              {
                  data: 'nama',
                  name: 'nama',
                  searchable: true
              },
              {
                  data: 'action',
                  name: 'action',
                  searchable: false
              }
          ]
      });
  });
</script>
<script>
    function edit(id, name, username, email) {
        $('#id_edit').val(id);
        $('#name_edit').val(name);
        $('#username_edit').val(username);
        $('#email_edit').val(email);
        $('#edit').modal('show');
    }
</script>
<script>
    function hapus(id) {
    
        Swal.fire({
            title: "Apakah Anda Yakin?",
            text: "Data tidak akan bisa dikembalikan lagi!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya yakin!'
            }).then((result) => {
            if (result.value) {
                var url = "{{url('/pegawai/delete')}}";
                $.post( url, { _token: "{{ csrf_token() }}", id: id })
                .done(function( data ) {
                    if (data.error) {
                        Swal.fire("Gagal", "Penghapusan gagal, Kesalahan DB!", "error");
                    } else {
                        Swal.fire("Berhasil", "Penghapusan berhasil!", "success");
                        location.reload();
                    }
                });
            } else {
                Swal.fire("Batalkan", "Penghapusan Dibatalkan!", "error");
            }
        });        
    }
</script>
@endsection