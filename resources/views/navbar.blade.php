<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="{{url('/')}}" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-copy"></i>
        <p>
          Master Aplikasi
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{url('/admin')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Admin</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{url('/pegawai')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Pegawai</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{url('/periode')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Periode</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-copy"></i>
        <p>
          Master Bobot Nilai
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{url('/bobot')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Bobot Penilaian</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Kesesuaian Bidang (30%)
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('/bobot/pendidikan')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Pendidikan (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/ipk')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>IPK (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/kesesuaian')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Kesesuaian (30%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/pengalaman')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengalaman Kerja (30%)</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Uji PSIKOTEST (20%)
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('/bobot/iq')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>IQ (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/kepribadian')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Kepribadian (25%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/gambar')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Gambar (25%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/kraep')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Kraeplien atau Pauli (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/wartegg')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>WARTEGG (10%)</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Wawancara Kerja (40%)
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('/bobot/mental')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Mental dan Kesiapan (30%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/positif')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Memiliki Fikiran Positif (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/visi')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Visi misi soal masa depan (10%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/inovasi')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Berinovasi dan Berkembang (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/pantang')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Pantang Menyerah (20%)</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Penampilan (10%)
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('/bobot/usia')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Usia (50%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/tinggi')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tinggi Badan (10%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/penampilan')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Penampilan (20%)</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/bobot/tanggungan')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tanggungan (20%)</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="{{url('/logout')}}" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt text-info"></i>
        <p>Logout</p>
      </a>
    </li>
  </ul>
</nav>