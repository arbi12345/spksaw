<?php 
use App\Models\User;
use App\Models\Penilaian;
use App\Models\Periode;
 ?>
@extends('index')
@section('title', 'Penilaian')
@section('content')
<div class="row">
  <div class="col-12">
    <h4>Penilaian Pegawai</h4>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <form method="post" class="role" action="{{url('/penilaian/add')}}" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label>Nama</label>
        <select class="select2 form-control" name="user_id">
          @foreach(User::where('access', '2')->get() as $user)
            <option value="{{$user->id}}">{{$user->nama}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Periode</label>
        <select class="select2 form-control" name="periode_id">
          @foreach(Periode::all() as $periode)
            <option value="{{$periode->id}}">{{$periode->nama}}</option>
          @endforeach
        </select>
      </div>

      <hr>
      <h5>Kesesuaian dengan bidang ilmu(30%):</h5>
      <hr>

      <div class="form-group">
        <label>Pendidikan (20%)</label>
        <select class="select2 form-control" name="pendidikan">
          <?php 
          use App\Models\BobotPendidikan;
           ?>
          @foreach(BobotPendidikan::all() as $pendidikan)
            <option value="{{$pendidikan->bobot_id}}">{{$pendidikan->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>IPK (20%)</label>
        <select class="select2 form-control" name="ipk">
          <?php 
          use App\Models\BobotIpk;
           ?>
          @foreach(BobotIpk::all() as $ipk)
            <option value="{{$ipk->bobot_id}}">{{$ipk->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Kesesuaian bidang ilmu (30%)</label>
        <select class="select2 form-control" name="kesesuaian">
          <?php 
          use App\Models\BobotKesesuaian;
           ?>
          @foreach(BobotKesesuaian::all() as $kesesuaian)
            <option value="{{$kesesuaian->bobot_id}}">{{$kesesuaian->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Pengalaman kerja (30%)</label>
        <select class="select2 form-control" name="pengalaman">
          <?php 
          use App\Models\BobotPengalaman;
           ?>
          @foreach(BobotPengalaman::all() as $pengalaman)
            <option value="{{$pengalaman->bobot_id}}">{{$pengalaman->name}}</option>
          @endforeach
        </select>
      </div>

      <hr>
      <h5>Hasil ujian tertulis psikotest(20%):</h5>
      <hr>

      <div class="form-group">
        <label>TES IQ (20%)</label>
        <select class="select2 form-control" name="iq">
          <?php 
          use App\Models\BobotTestIq;
           ?>
          @foreach(BobotTestIq::all() as $BobotTestIq)
            <option value="{{$BobotTestIq->bobot_id}}">{{$BobotTestIq->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>TES Kepribadian (25%)</label>
        <select class="select2 form-control" name="kepribadian">
          <?php 
          use App\Models\BobotTestKepribadian;
           ?>
          @foreach(BobotTestKepribadian::all() as $BobotTestKepribadian)
            <option value="{{$BobotTestKepribadian->bobot_id}}">{{$BobotTestKepribadian->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Tes Gambar(menunjukkan diri Anda yang bertanggungjawab, percaya diri, stabil danmemiliki ketahanan kerja)(25%)</label>
        <select class="select2 form-control" name="gambar">
          <?php 
          use App\Models\BobotTestGambar;
           ?>
          @foreach(BobotTestGambar::all() as $BobotTestGambar)
            <option value="{{$BobotTestGambar->bobot_id}}">{{$BobotTestGambar->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Tes Kraeplien atau Pauli(konsistensi dan ketahanan)(20%)</label>
        <select class="select2 form-control" name="kraep">
          <?php 
          use App\Models\BobotTestKraeplien;
           ?>
          @foreach(BobotTestKraeplien::all() as $BobotTestKraeplien)
            <option value="{{$BobotTestKraeplien->bobot_id}}">{{$BobotTestKraeplien->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Wartegg Test(imajinasi)(10%)</label>
        <select class="select2 form-control" name="wartegg">
          <?php 
          use App\Models\BobotTestWartegg;
           ?>
          @foreach(BobotTestWartegg::all() as $BobotTestWartegg)
            <option value="{{$BobotTestWartegg->bobot_id}}">{{$BobotTestWartegg->name}}</option>
          @endforeach
        </select>
      </div>

      <hr>
      <h5>Hasil wawancara(40%):</h5>
      <hr>

      <div class="form-group">
        <label>Mental dan Kesiapan Pelamar Kerja(30%)</label>
        <select class="select2 form-control" name="mental">
          <?php 
          use App\Models\BobotWawancaraMental;
           ?>
          @foreach(BobotWawancaraMental::all() as $BobotWawancaraMental)
            <option value="{{$BobotWawancaraMental->bobot_id}}">{{$BobotWawancaraMental->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Memiliki pikiran positif(20%)</label>
        <select class="select2 form-control" name="positif">
          <?php 
          use App\Models\BobotWawancaraPositif;
           ?>
          @foreach(BobotWawancaraPositif::all() as $BobotWawancaraPositif)
            <option value="{{$BobotWawancaraPositif->bobot_id}}">{{$BobotWawancaraPositif->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Visi misi pelamar soal masa depan(10%)</label>
        <select class="select2 form-control" name="visi">
          <?php 
          use App\Models\BobotWawancaraVisi;
           ?>
          @foreach(BobotWawancaraVisi::all() as $BobotWawancaraVisi)
            <option value="{{$BobotWawancaraVisi->bobot_id}}">{{$BobotWawancaraVisi->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Berinovasi dan berkembang(20%)</label>
        <select class="select2 form-control" name="inovasi">
          <?php 
          use App\Models\BobotWawancaraInovasi;
           ?>
          @foreach(BobotWawancaraInovasi::all() as $BobotWawancaraInovasi)
            <option value="{{$BobotWawancaraInovasi->bobot_id}}">{{$BobotWawancaraInovasi->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Sikap pantang menyerah(20%)</label>
        <select class="select2 form-control" name="pantang">
          <?php 
          use App\Models\BobotWawancaraPantang;
           ?>
          @foreach(BobotWawancaraPantang::all() as $BobotWawancaraPantang)
            <option value="{{$BobotWawancaraPantang->bobot_id}}">{{$BobotWawancaraPantang->name}}</option>
          @endforeach
        </select>
      </div>

      <hr>
      <h5>Penampilan (10%):</h5>
      <hr>

      <div class="form-group">
        <label>Usia(50%)</label>
        <select class="select2 form-control" name="usia">
          <?php 
          use App\Models\BobotPenampilanUsia;
           ?>
          @foreach(BobotPenampilanUsia::all() as $BobotPenampilanUsia)
            <option value="{{$BobotPenampilanUsia->bobot_id}}">{{$BobotPenampilanUsia->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Tinggi badan(10%)</label>
        <select class="select2 form-control" name="tinggi">
          <?php 
          use App\Models\BobotPenampilanTinggi;
           ?>
          @foreach(BobotPenampilanTinggi::all() as $BobotPenampilanTinggi)
            <option value="{{$BobotPenampilanTinggi->bobot_id}}">{{$BobotPenampilanTinggi->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Penampilan(20%)</label>
        <select class="select2 form-control" name="penampilan">
          <?php 
          use App\Models\BobotPenampilanPenampilan;
           ?>
          @foreach(BobotPenampilanPenampilan::all() as $BobotPenampilanPenampilan)
            <option value="{{$BobotPenampilanPenampilan->bobot_id}}">{{$BobotPenampilanPenampilan->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label>Tanggungan(20%)</label>
        <select class="select2 form-control" name="tanggungan">
          <?php 
          use App\Models\BobotPenampilanTanggungan;
           ?>
          @foreach(BobotPenampilanTanggungan::all() as $BobotPenampilanTanggungan)
            <option value="{{$BobotPenampilanTanggungan->bobot_id}}">{{$BobotPenampilanTanggungan->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Kirim">
      </div>

    </form>
  </div>
</div>
@endsection