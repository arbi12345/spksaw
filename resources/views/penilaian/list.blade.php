<?php 
use App\Models\Periode;
?>
@extends('index')
@section('title', 'Daftar Pegawai')
@section('content')
@if(Session('add'))
<script type="text/javascript">
    $(window).on('load',function(){
        Swal.fire({
        icon: 'success',
        title: 'Ok...',
        text: 'Data Berhasil Ditambahkan!',
        footer: '<a href>Tutup</a>'
        });
    });
</script>
@endif
@if(Session('edit'))
  <script type="text/javascript">
      // alert("Password Salah!");
      $(window).on('load',function(){
          Swal.fire({
          icon: 'success',
          title: 'Ok...',
          text: 'Data Berhasil Dirubah!',
          footer: '<a href>Tutup</a>'
          });
      });
  </script>
@endif

<!-- <button onclick="return getStudents(1);">Test</button> -->
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pegawai</h4>

          <button type="button" class="btn btn-info btn-rounded btn-fw" data-toggle="modal" data-target="#download" style="float: right;margin-right: 15px;margin-bottom: 15px;">Download Laporan</button>

          <table class="table table-striped" id="data_pegawai">
              <thead>
                  <tr>
                      <th> Nama </th>
                      <th> Aksi </th>
                  </tr>
              </thead>
          </table>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="download">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Download</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="form_category" action="{{url('/penilaian/report')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          
          <div class="form-group">
            <label>Periode</label>
            <select class="form-control select2" name="periode">
              <option value="0">Semua</option>
               @foreach(Periode::all() as $periode)
                <option value="{{$periode->id}}">{{$periode->nama}}</option>
               @endforeach
            </select>
          </div>

          <div class="form-group">
            <input type="submit" name="kirim" class="btn btn-success btn-lg" value="Download">
          </div>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" onclick="edit()" class="btn btn-success">Edit</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
<script>
    $(function() {
      $('#data_pegawai').DataTable({
          processing: true,
          serverSide: true,
          stateSave: true,
          ajax: "{{url('/penilaian/get')}}",
          oLanguage: {sProcessing: "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif' /><P style='margin-top:5px;color:blue;size:18px;'>Loading......</p>"},
          columns: [
              {
                  data: 'nama',
                  name: 'nama',
                  searchable: true
              },
              {
                  data: 'action',
                  name: 'action',
                  searchable: false
              }
          ]
      });
  });
</script>
<script>
    function download(id) {
      window.location.href = "{{url('/penilaian/download/')}}/" + id;        
    }
</script>
@endsection