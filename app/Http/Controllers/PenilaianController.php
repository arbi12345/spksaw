<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penilaian;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon as carbon;
use App\Exports\SingleReportExport as Export;
use App\Exports\AllReportExport as ExportAll;
use Auth;
use DataTables;
use File;
use Excel;
use PDF;
use Image;

class PenilaianController extends Controller
{
    public function index()
    {
        return view('penilaian.index');
    }

    public function list()
    {
        return view('penilaian.list');
    }

    public function get()
    {
        $admin = User::where('access', '2');

        $data = DataTables::eloquent($admin)
                ->editColumn('nama', function ($admin)
                {
                    return "<a href='" . url('/penilaian/detail/' . $admin->id) . "'>" . $admin->nama . "</a>";
                })
        		->addColumn("action", function ($admin)
                {
                   return "<button type='button' class='btn btn-success btn-rounded btn-block' onclick=\"download('" . $admin->id . "')\"> <i class='far fa-arrow-alt-circle-down'></i> Download</button>";
                })
                ->rawColumns(['nama', 'action'])
                ->make(true);
        return $data;
    }

    public function detail($id)
    {
    	$user = User::find($id);
    	return view('penilaian.detail', ['user' => $user]);
    }

    public function add(Request $request)
    {
    	$penilaian = Penilaian::where('user_id', $request->user)->where('periode_id', $request->periode_id)->first();
    	if (!isset($penilaian->id)) {
    		$penilaian = new Penilaian;
    	}
    	$penilaian->user_id = $request->user_id;
    	$penilaian->periode_id = $request->periode_id;
    	$penilaian->pendidikan = $request->pendidikan;
    	$penilaian->ipk = $request->ipk;
    	$penilaian->kesesuaian = $request->kesesuaian;
    	$penilaian->pengalaman = $request->pengalaman;
    	$penilaian->iq = $request->iq;
    	$penilaian->kepribadian = $request->kepribadian;
    	$penilaian->gambar = $request->gambar;
    	$penilaian->kraep = $request->kraep;
    	$penilaian->wartegg = $request->wartegg;
    	$penilaian->mental = $request->mental;
    	$penilaian->positif = $request->positif;
    	$penilaian->visi = $request->visi;
    	$penilaian->inovasi = $request->inovasi;
    	$penilaian->pantang = $request->pantang;
    	$penilaian->tinggi = $request->tinggi;
    	$penilaian->usia = $request->usia;
    	$penilaian->penampilan = $request->penampilan;
    	$penilaian->tanggungan = $request->tanggungan;
    	$penilaian->save();

        return redirect('/penilaian')->with('add', 'success');
    }
    public function download($id)
    {
    	$user = User::find($id);

    	$data = new Export(['user' => $user]);
        return Excel::download($data, 'Laporan ' . $user->nama . '.xlsx');
    }

    public function report(Request $request)
    {
		$data = new ExportAll(['periode' => $request->periode]);
    	return Excel::download($data, 'Laporan Semua.xlsx');
    }
}
