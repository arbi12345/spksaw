<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bobot;
use Carbon\Carbon as carbon;
use Auth;
use DataTables;
use File;
use Image;

class BobotController extends Controller
{
    public function index()
    {
        return view('bobot.index');
    }
    
    public function get()
    {
        $admin = Bobot::where('id', '<>', 0);

        // dd($admin->get());

        $data = DataTables::eloquent($admin)
        		->addColumn("action", function ($admin)
                {
                  $edit = 
                   "<button type='button' class='btn btn-warning btn-rounded btn-block' onclick=\"edit('" . $admin->id . "','" . $admin->nama . "','" . $admin->nilai . "')\"> <i class='fa fa-magic'></i> Edit</button>";
                  $delete = 
                       "<button type='button' class='btn btn-danger btn-rounded btn-block' onclick=\"hapus('" . $admin->id . "')\"> <i class='fa fa-trash'></i> Hapus</button>";
                   $action = $edit . " <hr> " . $delete;
                   return $action;
                })
                ->rawColumns(['nama', 'action'])
                ->make(true);
        return $data;
    }

    public function add(Request $request)
    {
        $bobot = new Bobot;
        $bobot->nama = $request->name;
        $bobot->nilai = $request->nilai;
        $bobot->save();

        return redirect('/bobot')->with('add', 'success');
    }

    public function edit(Request $request)
    {
        // phpinfo();

        $bobot = Bobot::find($request->id);
        $bobot->nama = $request->name;
        $bobot->nilai = $request->nilai;
        $bobot->save();

        return redirect('/bobot')->with('edit', 'success');
    }

    public function delete(Request $request)
    {
        $bobot = Bobot::find($request->id);
        $hapus = $bobot->delete();

        if ($hapus) {
            return response()->json(['error' => false, 'id' => $request->id]);
        } else {
            return response()->json(['error' => true, 'id' => $request->id]);
        }
    }
}
