<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Periode;
use Carbon\Carbon as carbon;
use Auth;
use DataTables;
use File;
use Image;

class MasterPeriodeController extends Controller
{
    public function index()
    {
        return view('periode.index');
    }
    
    public function get()
    {
        $admin = Periode::where('id' ,'<>' ,'0');

        // dd($admin->get());

        $data = DataTables::eloquent($admin)
                ->addColumn("action", function ($admin)
                {
                   $edit = 
                   "<button type='button' class='btn btn-warning btn-rounded btn-block' onclick=\"edit('" . $admin->id . "','" . $admin->nama . "','" . $admin->tahun . "')\"> <i class='fa fa-magic'></i> Edit</button>";
                   $delete = 
                       "<button type='button' class='btn btn-danger btn-rounded btn-block' onclick=\"hapus('" . $admin->id . "')\"> <i class='fa fa-trash'></i> Hapus</button>";
                   $action = $edit . " <hr> " . $delete;
                   return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        return $data;
    }

    public function add(Request $request)
    {
        $periode = new Periode;
        $periode->nama = $request->name;
        $periode->tahun = $request->tahun;
        $periode->save();

        return redirect('/admin')->with('add', 'success');
    }

    public function edit(Request $request)
    {
        // phpinfo();

        $periode = Periode::find($request->id);
        $periode->nama = $request->name;
        $periode->tahun = $request->tahun;
        $periode->save();

        return redirect('/admin')->with('add', 'success');
    }

    public function delete(Request $request)
    {
        $periode = Periode::find($request->id);
        $hapus = $periode->delete();

        if ($hapus) {
            return response()->json(['error' => false, 'id' => $request->id]);
        } else {
            return response()->json(['error' => true, 'id' => $request->id]);
        }
    }
}
