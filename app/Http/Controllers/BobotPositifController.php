<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BobotWawancaraPositif as Bobot;
use Carbon\Carbon as carbon;
use Auth;
use DataTables;
use File;
use Image;

class BobotPositifController extends Controller
{
    public function index()
    {
        return view('bobot.positif.index');
    }
    
    public function get()
    {
        $bobot = Bobot::with('bobot');

        // dd($bobot->get());

        $data = DataTables::eloquent($bobot)
        		->addColumn("nilai", function ($bobot)
                {
                  $result = $bobot->bobot->nama . " ( " . $bobot->bobot->nilai . ' )';
                  return $result;
                })
        		->addColumn("action", function ($bobot)
                {
                  $edit = 
                   "<button type='button' class='btn btn-warning btn-rounded btn-block' onclick=\"edit('" . $bobot->id . "','" . $bobot->name . "','" . $bobot->bobot_id . "')\"> <i class='fa fa-magic'></i> Edit</button>";
                  $delete = 
                       "<button type='button' class='btn btn-danger btn-rounded btn-block' onclick=\"hapus('" . $bobot->id . "')\"> <i class='fa fa-trash'></i> Hapus</button>";
                   $action = $edit . " <hr> " . $delete;
                   return $action;
                })
                ->rawColumns(['nama', 'action'])
                ->make(true);
        return $data;
    }

    public function add(Request $request)
    {
        $bobot = new Bobot;
        $bobot->name = $request->name;
        $bobot->bobot_id = $request->bobot_id;
        $bobot->save();

        return redirect('/bobot/positif')->with('add', 'success');
    }

    public function edit(Request $request)
    {
        // phpinfo();

        $bobot = Bobot::find($request->id);
        $bobot->name = $request->name;
        $bobot->bobot_id = $request->bobot_id;
        $bobot->save();

        return redirect('/bobot/positif')->with('edit', 'success');
    }

    public function delete(Request $request)
    {
        $bobot = Bobot::find($request->id);
        $hapus = $bobot->delete();

        if ($hapus) {
            return response()->json(['error' => false, 'id' => $request->id]);
        } else {
            return response()->json(['error' => true, 'id' => $request->id]);
        }
    }
}
