<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    public function login()
    {
    	return view('login');
    }

    public function process(Request $request)
    {
    	if(isset($request->remember))
	      {
	        $remember = true;
	      }
	    elseif(!isset($request->remember))
	      {
	        $remember = false;
	      }

	    $username = $request->username;
	    $password = Hash::make($request->password);
	    $user = User::where('username', $username)->first();
	    // dd($user);
	    if (!isset($user->password)) {
	        return redirect()->back()->withErrors(['password']);
	    }

	    $login = Hash::check($request->password, $user->password);
	    if($login) {
	      Auth::login($user, $remember);
	      return redirect('/');
	    } else {
	      return redirect('/login')->withErrors(['password']);
	    }
    }

    public function logout()
    {
      Auth::logout();

      return redirect('/login')->with('logout', 'success');
    }}
