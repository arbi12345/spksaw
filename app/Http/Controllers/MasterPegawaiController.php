<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Exports\PegawaiExport as Export;
use App\Imports\PegawaiImport as Import;
use App\Exports\PegawaiExportTemplate as Template;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon as carbon;
use Auth;
use DataTables;
use File;
use Excel;
use PDF;
use Image;

class MasterPegawaiController extends Controller
{
    public function index()
    {
    	// dd(['hoya']);
        return view('pegawai.index');
    }

    public function single($id)
    {
        return view('pegawai.detail', ['user' => User::find($id)]);
    }
    
    public function get()
    {
        $admin = User::where('access', '2');

        // dd($admin->get());

        $data = DataTables::eloquent($admin)
                ->editColumn('nama', function ($admin)
                {
                    return "<a href='" . url('/pegawai/detail/' . $admin->id) . "'>" . $admin->nama . "</a>";
                })
        		->addColumn("action", function ($admin)
                {
                   $edit = 
                   "<button type='button' class='btn btn-warning btn-rounded btn-block' onclick=\"edit('" . $admin->id . "','" . $admin->nama . "','" . $admin->username . "','" . $admin->email . "')\"> <i class='fa fa-magic'></i> Edit</button>";
                   if ($admin->id != Auth::user()->id) {
                        $delete = 
                       "<button type='button' class='btn btn-danger btn-rounded btn-block' onclick=\"hapus('" . $admin->id . "')\"> <i class='fa fa-trash'></i> Hapus</button>";
                   } else {
                        $delete = "";
                   }
                   $action = $edit . " <hr> " . $delete;
                   return $action;
                })
                ->rawColumns(['nama', 'action'])
                ->make(true);
        return $data;
    }

    public function add(Request $request)
    {
        $user = new User;
        $user->nama = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->access = 2;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/pegawai')->with('add', 'success');
    }

    public function edit(Request $request)
    {
        // phpinfo();

        $user = User::find($request->id);
        $user->nama = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect('/pegawai')->with('add', 'success');
    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);
        $hapus = $user->delete();

        if ($hapus) {
            return response()->json(['error' => false, 'id' => $request->id]);
        } else {
            return response()->json(['error' => true, 'id' => $request->id]);
        }
    }

    public function upload(Request $request)
    {
    	$rows = Excel::toCollection(new Import, request()->file('import'));
    	// dd($rows[0]);
        $cek = [];
    	foreach ($rows[0] as $row) 
        {
        	if ($row['username'] != null AND $row['email'] != null  AND $row['nama'] != null  AND $row['password'] != null ) {
                $user = User::where('username', strval($row['username']))->first();
                if (!isset($user->id)) {
                    $user = new User;
                } 

	        	$user->username = $row['username'];
	        	$user->email = $row['email'];
	        	$user->nama = $row['nama'];
                $user->password = Hash::make($row['password']);
	        	$user->access = 2;
	        	$user->save();
        	}
        }
        // dd($cek);
    	return redirect('/pegawai')->with('add', 'success');
    }

    public function download(Request $request)
    {
    	$user = User::where('access', '2')->get();

    	if ($request->format == 'excel') {
    		$data = new Export(['users' => $user]);
        	return Excel::download($data, 'laporan_pegawai.xlsx');
    	} elseif ($request->format == 'pdf') {
    		$pdf = PDF::loadView('export.pegawai', ['users' => $user]);
    		return $pdf->download('laporan_pegawai.pdf');
    	} else {
    		return view('export.pegawai', ['users' => $user]);
    	}
    }

    public function export()
    {
		$data = new Template;
    	return Excel::download($data, 'format_pegawai.xlsx');
    }
}
