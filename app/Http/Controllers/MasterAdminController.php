<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon as carbon;
use Auth;
use DataTables;
use File;
use Image;

class MasterAdminController extends Controller
{
    public function index()
    {
    	// dd(['hoya']);
        return view('admin.index');
    }

    public function single($id)
    {
        return view('admin.detail', ['user' => User::find($id)]);
    }
    
    public function get()
    {
        $admin = User::where('access', '1');

        // dd($admin->get());

        $data = DataTables::eloquent($admin)
                ->editColumn('nama', function ($admin)
                {
                    return "<a href='" . url('/admin/detail/' . $admin->id) . "'>" . $admin->nama . "</a>";
                })
        		->addColumn("action", function ($admin)
                {
                   $edit = 
                   "<button type='button' class='btn btn-warning btn-rounded btn-block' onclick=\"edit('" . $admin->id . "','" . $admin->nama . "','" . $admin->username . "','" . $admin->email . "')\"> <i class='fa fa-magic'></i> Edit</button>";
                   if ($admin->id != Auth::user()->id) {
                        $delete = 
                       "<button type='button' class='btn btn-danger btn-rounded btn-block' onclick=\"hapus('" . $admin->id . "')\"> <i class='fa fa-trash'></i> Hapus</button>";
                   } else {
                        $delete = "";
                   }
                   $action = $edit . " <hr> " . $delete;
                   return $action;
                })
                ->rawColumns(['nama', 'action'])
                ->make(true);
        return $data;
    }

    public function add(Request $request)
    {
        $user = new User;
        $user->nama = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->access = 1;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/admin')->with('add', 'success');
    }

    public function edit(Request $request)
    {
        // phpinfo();

        $user = User::find($request->id);
        $user->nama = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect('/admin')->with('add', 'success');
    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);
        $hapus = $user->delete();

        if ($hapus) {
            return response()->json(['error' => false, 'id' => $request->id]);
        } else {
            return response()->json(['error' => true, 'id' => $request->id]);
        }
    }
}
