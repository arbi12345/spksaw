<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BobotTestIq extends Model
{
    use HasFactory;

    public function bobot()
    {
    	return $this->belongsTo('App\Models\Bobot');
    }
}
