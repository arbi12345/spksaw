<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaians', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('periode_id');
            $table->integer('pendidikan');
            $table->integer('ipk');
            $table->integer('kesesuaian');
            $table->integer('pengalaman');
            $table->integer('iq');
            $table->integer('kepribadian');
            $table->integer('gambar');
            $table->integer('kraep');
            $table->integer('wartegg');
            $table->integer('mental');
            $table->integer('positif');
            $table->integer('visi');
            $table->integer('inovasi');
            $table->integer('pantang');
            $table->integer('tinggi');
            $table->integer('usia');
            $table->integer('penampilan');
            $table->integer('tanggungan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaians');
    }
}
