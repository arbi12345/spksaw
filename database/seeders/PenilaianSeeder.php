<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Penilaian;
use App\Models\Periode;
use App\Models\User;

class PenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Periode::all() as $periode) {
        	foreach (User::where('access', '2')->get() as $user) {
	        	$penilaian = new Penilaian;
	        	$penilaian->user_id = $user->id;
	        	$penilaian->periode_id = $periode->id;
	        	$penilaian->pendidikan = rand(1,5);
	        	$penilaian->ipk = rand(1,5);
	        	$penilaian->kesesuaian = rand(1,5);
	        	$penilaian->pengalaman = rand(1,5);
	        	$penilaian->iq = rand(1,5);
	        	$penilaian->kepribadian = rand(1,5);
	        	$penilaian->gambar = rand(1,5);
	        	$penilaian->kraep = rand(1,5);
	        	$penilaian->wartegg = rand(1,5);
	        	$penilaian->mental = rand(1,5);
	        	$penilaian->positif = rand(1,5);
	        	$penilaian->visi = rand(1,5);
	        	$penilaian->inovasi = rand(1,5);
	        	$penilaian->pantang = rand(1,5);
	        	$penilaian->tinggi = rand(1,5);
	        	$penilaian->usia = rand(1,5);
	        	$penilaian->penampilan = rand(1,5);
	        	$penilaian->tanggungan = rand(1,5);
	        	$penilaian->save();
	        }
        }
    }
}
