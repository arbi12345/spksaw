<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Periode;

class PeriodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tahuns = [2015,2016,2017,2018,2019,2020,2021];
        foreach ($tahuns as $tahun) {
        	$periode = new Periode;
        	$periode->nama = "Periode Tahun " . $tahun;
        	$periode->tahun = $tahun;
        	$periode->save();
        }
    }
}
