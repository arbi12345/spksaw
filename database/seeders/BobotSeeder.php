<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Bobot;

class BobotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bobot = new Bobot;
        $bobot->id   = 1;
        $bobot->nama = "Sangat Kurang";
        $bobot->nilai= 0.20;
        $bobot->save();


        $bobot = new Bobot;
        $bobot->id   = 2;
        $bobot->nama = "Kurang";
        $bobot->nilai= 0.40;
        $bobot->save();


        $bobot = new Bobot;
        $bobot->id   = 3;
        $bobot->nama = "Cukup";
        $bobot->nilai= 0.60;
        $bobot->save();


        $bobot = new Bobot;
        $bobot->id   = 4;
        $bobot->nama = "Tinggi";
        $bobot->nilai= 0.80;
        $bobot->save();


        $bobot = new Bobot;
        $bobot->id   = 5;
        $bobot->nama = "Sangat Tinggi";
        $bobot->nilai= 1.00;
        $bobot->save();
    }
}
