<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
        for ($i=1; $i < 5; $i++) { 
        	$user = new User;
        	$user->nama = $faker->name;
        	$user->username = "admin_" . $i;
        	$user->email = $faker->email;
        	$user->password = Hash::make('123');
        	$user->access = 1;
        	$user->save();
        }
        for ($i=1; $i < 100; $i++) { 
        	$user = new User;
        	$user->nama = $faker->name;
        	$user->username = "user_" . $i;
        	$user->email = $faker->email;
        	$user->password = Hash::make('123');
        	$user->access = 2;
        	$user->save();
        }
    }
}
