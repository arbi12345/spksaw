<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthController@login')->name('login');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::post('/login/process', 'AuthController@process')->name('login.process');
Route::middleware('auth')->group(function ()
{
	Route::get('/', 'HomeController@index');

	Route::prefix('admin')->group(function ()
	{
		Route::get('/', 'MasterAdminController@index');
		Route::get('/detail/{id}', 'MasterAdminController@single');
		Route::get('/get', 'MasterAdminController@get');
		Route::post('/add', 'MasterAdminController@add');
		Route::post('/edit', 'MasterAdminController@edit');
		Route::post('/delete', 'MasterAdminController@delete');
	});

	Route::prefix('pegawai')->group(function ()
	{
		Route::get('/', 'MasterPegawaiController@index');
		Route::get('/detail/{id}', 'MasterPegawaiController@single');
		Route::get('/get', 'MasterPegawaiController@get');
		Route::post('/add', 'MasterPegawaiController@add');
		Route::post('/edit', 'MasterPegawaiController@edit');
		Route::post('/delete', 'MasterPegawaiController@delete');
		Route::post('/upload', 'MasterPegawaiController@upload');
		Route::post('/download', 'MasterPegawaiController@download');
		Route::get('/export', 'MasterPegawaiController@export');
	});

	Route::prefix('periode')->group(function ()
	{
		Route::get('/', 'MasterPeriodeController@index');
		Route::get('/get', 'MasterPeriodeController@get');
		Route::post('/add', 'MasterPeriodeController@add');
		Route::post('/edit', 'MasterPeriodeController@edit');
		Route::post('/delete', 'MasterPeriodeController@delete');
	});

	Route::prefix('bobot')->group(function ()
	{
		Route::get('/', 'BobotController@index');
		Route::get('/get', 'BobotController@get');
		Route::post('/add', 'BobotController@add');
		Route::post('/edit', 'BobotController@edit');
		Route::post('/delete', 'BobotController@delete');

		Route::prefix('pendidikan')->group(function ()
		{
			Route::get('/', 'BobotPendidikanController@index');
			Route::get('/get', 'BobotPendidikanController@get');
			Route::post('/add', 'BobotPendidikanController@add');
			Route::post('/edit', 'BobotPendidikanController@edit');
			Route::post('/delete', 'BobotPendidikanController@delete');
		});

		Route::prefix('ipk')->group(function ()
		{
			Route::get('/', 'BobotIpkController@index');
			Route::get('/get', 'BobotIpkController@get');
			Route::post('/add', 'BobotIpkController@add');
			Route::post('/edit', 'BobotIpkController@edit');
			Route::post('/delete', 'BobotIpkController@delete');
		});

		Route::prefix('kesesuaian')->group(function ()
		{
			Route::get('/', 'BobotKesesuaianController@index');
			Route::get('/get', 'BobotKesesuaianController@get');
			Route::post('/add', 'BobotKesesuaianController@add');
			Route::post('/edit', 'BobotKesesuaianController@edit');
			Route::post('/delete', 'BobotKesesuaianController@delete');
		});

		Route::prefix('pengalaman')->group(function ()
		{
			Route::get('/', 'BobotPengalamanController@index');
			Route::get('/get', 'BobotPengalamanController@get');
			Route::post('/add', 'BobotPengalamanController@add');
			Route::post('/edit', 'BobotPengalamanController@edit');
			Route::post('/delete', 'BobotPengalamanController@delete');
		});

		Route::prefix('iq')->group(function ()
		{
			Route::get('/', 'BobotIqController@index');
			Route::get('/get', 'BobotIqController@get');
			Route::post('/add', 'BobotIqController@add');
			Route::post('/edit', 'BobotIqController@edit');
			Route::post('/delete', 'BobotIqController@delete');
		});

		Route::prefix('kepribadian')->group(function ()
		{
			Route::get('/', 'BobotKepribadianController@index');
			Route::get('/get', 'BobotKepribadianController@get');
			Route::post('/add', 'BobotKepribadianController@add');
			Route::post('/edit', 'BobotKepribadianController@edit');
			Route::post('/delete', 'BobotKepribadianController@delete');
		});

		Route::prefix('gambar')->group(function ()
		{
			Route::get('/', 'BobotGambarController@index');
			Route::get('/get', 'BobotGambarController@get');
			Route::post('/add', 'BobotGambarController@add');
			Route::post('/edit', 'BobotGambarController@edit');
			Route::post('/delete', 'BobotGambarController@delete');
		});

		Route::prefix('kraep')->group(function ()
		{
			Route::get('/', 'BobotKraepController@index');
			Route::get('/get', 'BobotKraepController@get');
			Route::post('/add', 'BobotKraepController@add');
			Route::post('/edit', 'BobotKraepController@edit');
			Route::post('/delete', 'BobotKraepController@delete');
		});

		Route::prefix('wartegg')->group(function ()
		{
			Route::get('/', 'BobotWarteggController@index');
			Route::get('/get', 'BobotWarteggController@get');
			Route::post('/add', 'BobotWarteggController@add');
			Route::post('/edit', 'BobotWarteggController@edit');
			Route::post('/delete', 'BobotWarteggController@delete');
		});

		Route::prefix('mental')->group(function ()
		{
			Route::get('/', 'BobotMentalController@index');
			Route::get('/get', 'BobotMentalController@get');
			Route::post('/add', 'BobotMentalController@add');
			Route::post('/edit', 'BobotMentalController@edit');
			Route::post('/delete', 'BobotMentalController@delete');
		});

		Route::prefix('positif')->group(function ()
		{
			Route::get('/', 'BobotPositifController@index');
			Route::get('/get', 'BobotPositifController@get');
			Route::post('/add', 'BobotPositifController@add');
			Route::post('/edit', 'BobotPositifController@edit');
			Route::post('/delete', 'BobotPositifController@delete');
		});

		Route::prefix('visi')->group(function ()
		{
			Route::get('/', 'BobotVisiController@index');
			Route::get('/get', 'BobotVisiController@get');
			Route::post('/add', 'BobotVisiController@add');
			Route::post('/edit', 'BobotVisiController@edit');
			Route::post('/delete', 'BobotVisiController@delete');
		});

		Route::prefix('inovasi')->group(function ()
		{
			Route::get('/', 'BobotInovasiController@index');
			Route::get('/get', 'BobotInovasiController@get');
			Route::post('/add', 'BobotInovasiController@add');
			Route::post('/edit', 'BobotInovasiController@edit');
			Route::post('/delete', 'BobotInovasiController@delete');
		});

		Route::prefix('pantang')->group(function ()
		{
			Route::get('/', 'BobotPantangController@index');
			Route::get('/get', 'BobotPantangController@get');
			Route::post('/add', 'BobotPantangController@add');
			Route::post('/edit', 'BobotPantangController@edit');
			Route::post('/delete', 'BobotPantangController@delete');
		});

		Route::prefix('usia')->group(function ()
		{
			Route::get('/', 'BobotUsiaController@index');
			Route::get('/get', 'BobotUsiaController@get');
			Route::post('/add', 'BobotUsiaController@add');
			Route::post('/edit', 'BobotUsiaController@edit');
			Route::post('/delete', 'BobotUsiaController@delete');
		});

		Route::prefix('tinggi')->group(function ()
		{
			Route::get('/', 'BobotTinggiController@index');
			Route::get('/get', 'BobotTinggiController@get');
			Route::post('/add', 'BobotTinggiController@add');
			Route::post('/edit', 'BobotTinggiController@edit');
			Route::post('/delete', 'BobotTinggiController@delete');
		});

		Route::prefix('penampilan')->group(function ()
		{
			Route::get('/', 'BobotPenampilanController@index');
			Route::get('/get', 'BobotPenampilanController@get');
			Route::post('/add', 'BobotPenampilanController@add');
			Route::post('/edit', 'BobotPenampilanController@edit');
			Route::post('/delete', 'BobotPenampilanController@delete');
		});

		Route::prefix('tanggungan')->group(function ()
		{
			Route::get('/', 'BobotTanggunganController@index');
			Route::get('/get', 'BobotTanggunganController@get');
			Route::post('/add', 'BobotTanggunganController@add');
			Route::post('/edit', 'BobotTanggunganController@edit');
			Route::post('/delete', 'BobotTanggunganController@delete');
		});
	});

	Route::prefix('penilaian')->group(function ()
	{
		Route::get('/', 'PenilaianController@index');
		Route::get('/list', 'PenilaianController@list');
		Route::get('/get', 'PenilaianController@get');
		Route::get('/download/{id}', 'PenilaianController@download');
		Route::get('/detail/{id}', 'PenilaianController@detail');
		Route::post('/report', 'PenilaianController@report');
		Route::post('/add', 'PenilaianController@add');
	});
});
